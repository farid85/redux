import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import { Layout } from "antd";
import Header1 from "./components/Header";
import ProductDetail from "./components/ProductDetail";
import ProductListing from "./components/ProductListing";

const { Footer, Sider, Content } = Layout;

function App() {
  return (
    <BrowserRouter>
      <div className="wrapper">
        <Layout>
          <Header1 />
          <Routes>
            <Route path="/" element={<ProductListing />} />
            <Route path="/product/:id" element={<ProductDetail />} />
          </Routes>
        </Layout>
      </div>
    </BrowserRouter>
  );
}

export default App;
