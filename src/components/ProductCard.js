import React from "react";
import { Card } from "antd";
import { useSelector } from "react-redux";
import "../App.css";
import { Link } from "react-router-dom";

const { Meta } = Card;

const ProductCard = () => {
  const products = useSelector((state) => state.products.products);
  return (
    <>
      {products.length ===0 ?<h1>Loading...</h1> :products.map((product) => (
        <div key={product.id}>
          <Link to={`/product/${product.id}`}>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img alt={product.title} src={product.image} height="200" />
              }
            >
              <Meta
                title={product.title}
                description={product.description.slice(0, 50)}
              />
              <Meta
                title={product.price + "$"}
                description={product.category}
              />
            </Card>
          </Link>
        </div>
      ))}
    </>
  );
};

export default ProductCard;
