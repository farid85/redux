import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  removeSelectedProduct,
  selectedProduct,
} from "../redux/actions/productActions";

const ProductDetail = () => {
  const product = useSelector((state) => state.product);
  const { id } = useParams();
  const dispatch = useDispatch();

  const getProduct = async (id) => {
    try {
      const response = await axios.get(
        `https://fakestoreapi.com/products/${id}`
      );
      dispatch(selectedProduct(response.data));
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(id);
  useEffect(() => {
    if (id && id !== "") getProduct(id);

    return () => {
      dispatch(removeSelectedProduct());
    };
  }, [id]);
  return (
    <>
      {Object.keys(product).length === 0 ? (
        <div>...Loading</div>
      ) : (
        <div>
          <h2>{product.title}</h2>
          <img src={product.image} alt="" width="400" height="300" /> <br />
          <b>{product.price}</b>
          <article>{product.description}</article>
        </div>
      )}
    </>
  );
};

export default ProductDetail;
