import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "./ProductCard";
import { setProducts } from "../redux/actions/productActions";

const ProductListing = () => {
  const products = useSelector((state) => state.products.products);
  const dispatch = useDispatch();

  console.log(products);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await axios.get("https://fakestoreapi.com/products");
      console.log(response);
      dispatch(setProducts(response.data));
      
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="products">
          <ProductCard />
    </div>
  );
};

export default ProductListing;
